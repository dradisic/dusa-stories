<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/new', [
    'uses' => 'PageController@createNew'
]);
Auth::routes();

Route::get('/', [
    'uses' => 'FrontendController@index',
    'as' => 'index'
]);

Route::get('/{slug}', [
    'uses' => 'FrontendController@singlePost',
    'as' => 'posts.single'
]);

Route::post('/comment/store/{slug}', [
    'uses' => 'CommentController@store',
    'as' => 'comment.store'
]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){

//    Post
    Route::get('/dashboard', [
        'uses' => 'PostController@index',
        'as' => 'post.index'
    ]);
    Route::get('/post/trashed', [
        'uses' => 'PostController@trashed',
        'as' => 'post.trashed'
    ]);
    Route::get('/post/kill/{id}', [
        'uses' => 'PostController@kill',
        'as' => 'post.kill'
    ]);
    Route::get('/post/restore/{id}', [
        'uses' => 'PostController@restore',
        'as' => 'post.restore'
    ]);
    Route::get('/post/create', [
        'uses' => 'PostController@create',
        'as' => 'post.create'
    ]);
    Route::post('/post/store', [
        'uses' => 'PostController@store',
        'as' => 'post.store'
    ]);
    Route::post('/post/update/{id}', [
        'uses' => 'PostController@update',
        'as' => 'post.update'
    ]);
    Route::get('/post/edit/{id}', [
        'uses' => 'PostController@edit',
        'as' => 'post.edit'
    ]);
    Route::get('/post/delete/{id}', [
        'uses' => 'PostController@destroy',
        'as' => 'post.delete'
    ]);

//    Category
    Route::get('/category/create', [
        'uses' => 'CategoryController@create',
        'as' => 'category.create'
    ]);
    Route::post('/category/store', [
        'uses' => 'CategoryController@store',
        'as' => 'category.store'
    ]);
    Route::get('/categories', [
        'uses' => 'CategoryController@index',
        'as' => 'categories'
    ]);

    Route::post('/category/update/{id}', [
        'uses' => 'CategoryController@update',
        'as' => 'category.update'
    ]);

    Route::get('/category/edit/{id}', [
        'uses' => 'CategoryController@edit',
        'as' => 'category.edit'
    ]);
    Route::get('/category/delete/{id}', [
        'uses' => 'CategoryController@destroy',
        'as' => 'category.delete'
    ]);

//    Tag
    Route::get('/tag/create', [
        'uses' => 'TagController@create',
        'as' => 'tag.create'
    ]);
    Route::post('/tag/store', [
        'uses' => 'TagController@store',
        'as' => 'tag.store'
    ]);
    Route::get('/tags', [
        'uses' => 'TagController@index',
        'as' => 'tags'
    ]);
    Route::get('/tag/trashed', [
        'uses' => 'TagController@trashed',
        'as' => 'tag.trashed'
    ]);
    Route::get('/tag/kill/{id}', [
        'uses' => 'TagController@kill',
        'as' => 'tag.kill'
    ]);
    Route::get('/tag/restore/{id}', [
        'uses' => 'TagController@restore',
        'as' => 'tag.restore'
    ]);
    Route::post('/tag/update/{id}', [
        'uses' => 'TagController@update',
        'as' => 'tag.update'
    ]);

    Route::get('/tag/edit/{id}', [
        'uses' => 'TagController@edit',
        'as' => 'tag.edit'
    ]);
    Route::get('/tag/delete/{id}', [
        'uses' => 'TagController@destroy',
        'as' => 'tag.delete'
    ]);

//    User
    Route::get('/users', [
        'uses' => 'UserController@index',
        'as' => 'users'
    ]);

    Route::get('/user/create', [
        'uses' => 'UserController@create',
        'as' => 'user.create'
    ]);
    Route::post('/user/store', [
        'uses' => 'UserController@store',
        'as' => 'user.store'
    ]);
    Route::get('/user/trashed', [
        'uses' => 'UserController@trashed',
        'as' => 'user.trashed'
    ]);
    Route::get('/user/kill/{id}', [
        'uses' => 'UserController@kill',
        'as' => 'user.kill'
    ]);
    Route::get('/user/restore/{id}', [
        'uses' => 'UserController@restore',
        'as' => 'user.restore'
    ]);
    Route::post('/user/update/{id}', [
        'uses' => 'UserController@update',
        'as' => 'user.update'
    ]);
    Route::get('/user/edit/{id}', [
        'uses' => 'UserController@edit',
        'as' => 'user.edit'
    ]);
    Route::get('/user/delete/{id}', [
        'uses' => 'UserController@destroy',
        'as' => 'user.delete'
    ]);
    Route::get('/user/admin/{id}', [
        'uses' => 'UserController@admin',
        'as' => 'user.admin'
    ]);
    Route::get('/user/not-admin/{id}', [
        'uses' => 'UserController@notAdmin',
        'as' => 'user.not.admin'
    ]);

    //Profile
    Route::get('/user/profile', [
        'uses' => 'ProfileController@index',
        'as' => 'user.profile'
    ]);
    Route::post('/user/profile/update', [
        'uses' => 'ProfileController@update',
        'as' => 'user.profile.update'
    ]);

    //Setting
    Route::get('/settings', [
        'uses' => 'SettingController@index',
        'as' => 'settings'
    ]);
    Route::post('/settings/update', [
        'uses' => 'SettingController@update',
        'as' => 'settings.update'
    ]);

});

