<?php
return [
    'newsletter_header' => 'Prijavite se na naš Newsletter!',
    'newsletter_subscribe_label' => 'prijavi se',
    'newsletter_subscribe_info' => 'Podite obavesteni prilikom objave priče',
    'email_placeholder' => 'Vaša email adresa',
    'footer_contact_info' => 'Pišite autorki direktno',
    'welcome' => 'Welcome to our application'
];