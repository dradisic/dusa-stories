@extends('layouts.frontend')

@section('content')
    <div class="content-wrapper">

        @include('includes.header')

        <div class="header-spacer"></div>

        <div class="container">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    @include('includes.post',  ['post' => $first_post])
                </div>
                <div class="col-lg-2"></div>
            </div>

            <div class="row">

                <div class="col-lg-6">
                    @include('includes.post',  ['post' => $second_post])
                </div>
                <div class="col-lg-6">
                    @include('includes.post',  ['post' => $third_post])
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row medium-padding120 bg-border-color">
                <div class="container">
                    <div class="col-lg-12">
                        @foreach($categories as $category)
                            @if($category->posts->count())
                                <div class="offers">
                                    <div class="row">
                                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                            <div class="heading">
                                                <h4 class="h1 heading-title">{{ $category->name }}</h4>
                                                <div class="heading-line">
                                                    <span class="short-line"></span>
                                                    <span class="long-line"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="case-item-wrap">
                                            @foreach($category->posts()->orderBy('created_at', 'desc')->take(3)->get() as $post)
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                    <div class="case-item">
                                                        <div class="case-item__thumb">
                                                            <img src="{{ $post->featured }}" alt="{{ $post->title }}">
                                                        </div>
                                                        <h6 class="case-item__title">
                                                            <a href="{{ $post->featured }}">
                                                                {{ $post->title }}
                                                            </a>
                                                        </h6>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="padded-50"></div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
