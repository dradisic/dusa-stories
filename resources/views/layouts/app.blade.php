<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('app/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('app/css/toastr.min.css') }}" rel="stylesheet">
    <script>
		window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <!-- include summernote css -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Dusicine Price') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false" aria-haspopup="true">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            @if(Auth::check())
                <div class="col-lg-4">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="{{ route('post.index') }}">
                                Pocetna
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('categories') }}">Kategorije</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('tags') }}">Tags</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('tag.trashed') }}">Obrisani tagovi</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('tag.create') }}">Napravi tag</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('post.trashed') }}">Obrisane price</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('category.create') }}">Napravi Kategoriju</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('post.create') }}">Napravi Pricu</a>
                        </li>
                        @if(Auth::user()->admin)
                            <li class="list-group-item">
                                <a href="{{ route('users') }}">Korisnici</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('user.create') }}">Kreiraj korisnika</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('settings') }}">Podesavanja</a>
                            </li>
                        @endif
                        <li class="list-group-item">
                            <a href="{{ route('user.profile') }}">Moj profile</a>
                        </li>
                    </ul>
                </div>
            @endif
            <div class="col-lg-8">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('app/js/app.js') }}"></script>
<script src="{{ asset('app/js/toastr.min.js') }}"></script>
<script>
    @if(Session::has('success'))
	toastr.success("{{ Session::get('success') }}")
    @endif
    @if(Session::has('info'))
	toastr.success("{{ Session::get('info') }}")
    @endif
</script>

<!-- include summernote js -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
	$('#content').summernote({
		placeholder: 'Sadrzaj price',
		tabsize: 2,
		height: 100
	});
</script>
</body>
</html>
