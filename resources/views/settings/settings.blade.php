@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')

    <div class="panel panel-default">
        <div class="panel-heading">
            Izmeni podesavanja
        </div>
        <div class="panel-body">
            <form action="{{ route('settings.update', ['id'=> $setting->id] ) }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="site_name">Ime sajta</label>
                    <input type="text" value="{{ $setting->site_name }}" name="site_name" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="contact_email">Kontakt email</label>
                    <input type="email" value="{{ $setting->contact_email }}" name="contact_email" class="form-control"/>
                </div>


                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Izmeni</button>
                </div>
            </form>
        </div>
    </div>

@stop