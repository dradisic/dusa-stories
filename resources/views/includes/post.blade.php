<article class="hentry post post-standard has-post-thumbnail sticky">

    <div class="post-thumb">
        <img src="{{ $post->featured }}" alt="{{ $post->title }}">
        <div class="overlay"></div>
        <a href="{{ route('posts.single', ['slug' => $post->slug]) }}" class="link-image js-zoom-image">
            <i class="seoicon-zoom"></i>
        </a>
        <a href="#" class="link-post">
            <i class="seoicon-link-bold"></i>
        </a>
    </div>

    <div class="post__content">

        <div class="post__content-info">

            <h2 class="post__title entry-title ">
                <a href="{{ route('posts.single', ['slug' => $post->slug]) }}">{{ $post->title }}</a>
            </h2>

            <div class="post-additional-info">
                <span class="post__date">
                    <i class="seoicon-clock"></i>
                    <time class="published" datetime="2016-04-17 12:00:00">
                        {{ $post->created_at->toFormattedDateString() }}
                    </time>
                </span>

                    <span class="category">
                        <i class="seoicon-tags"></i>
                            <a href="#">
                                {{ $post->category()->first()->name }}
                            </a>
                    </span>

                <span class="post__comments">
                    <a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i></a>
                    {{ $post->totalCommentCount() }}
                </span>

            </div>
        </div>
    </div>

</article>