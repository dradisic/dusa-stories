@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')

    <div class="panel panel-default">
        <div class="panel-heading">
            Izmeni tag: {{ $tag->tag }}
        </div>
        <div class="panel-body">
            <form action="{{ route('tag.update', ['id'=> $tag->id] ) }}" method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="tag">Tag</label>
                    <input type="text" value="{{ $tag->tag  }}" name="tag" class="form-control"/>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Izmeni</button>
                </div>
            </form>
        </div>
    </div>

@stop