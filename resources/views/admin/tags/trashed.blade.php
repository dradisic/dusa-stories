@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Obrisane priče
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <th>
                    Tag
                </th>
                <th></th>
                <th></th>
                </thead>
                <tbody>
                @if($tags->count())
                    @foreach($tags as $tag)
                        <tr>
                            <td>
                                {{ $tag->tag }}
                            </td>
                            <td>
                                <a href="{{ route('tag.restore', ['id'=> $tag->id] ) }}" class="btn btn-success">
                                    povrati
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('tag.kill', ['id'=> $tag->id] ) }}" class="btn btn-danger">
                                    obrisi
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">Nema tagova</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop