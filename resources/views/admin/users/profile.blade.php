@extends('layouts.app')

@section('content')
    @if(count($errors) > 0)
        <ul class="list-group">
            @foreach($errors->all() as $error)
                <li class="list-group-item text-danger">
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">
            Izmenite Vas profil
        </div>

        <div class="panel-body">
            <form action="{{ route('user.profile.update') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">Korisnik</label>
                    <input type="text" name="name" value="{{ $user->name }}" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" value="{{ $user->email }}" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="password">Novi password</label>
                    <input type="password" name="password" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="avatar">Avatar</label>
                    <input type="file" name="avatar" value="{{ $user->profile->avatar }}" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="about">O meni</label>
                    <textarea name="about" id="about" cols="10" rows="5"
                              class="form-control">{{ $user->profile->about }}</textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Izmeni</button>
                </div>
            </form>
        </div>
    </div>
@stop