@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Korisnici
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <th>
                    Slika
                </th>
                <th>
                    Ime
                </th>
                <th>
                    Dozvole
                </th>
                <th>
                    Brisanje
                </th>
                </thead>
                <tbody>
                @if($users->count())
                    @foreach($users as $user)
                        <tr>
                            <td>
                                <img src="{{ asset($user->profile->avatar) }}" height="45px" alt="">
                            </td>
                            <td>
                                {{ $user->name }}
                            </td>
                            <td>
                                @if($user->admin)
                                    <a href="{{ route('user.not.admin', ['id'=> $user->id] ) }}" class="btn btn-danger">
                                        Ukloni admina dozvole
                                    </a>
                                @else
                                    <a href="{{ route('user.admin', ['id'=> $user->id] ) }}" class="btn btn-sucess">
                                        Setuj admina dozvole
                                    </a>
                                @endif
                            </td>
                            @if(Auth::id() != $user->id)
                                <td>
                                    <a href="{{ route('user.delete', ['id'=> $user->id] ) }}" class="btn btn-danger">
                                        Obrisi
                                    </a>
                                </td>
                            @endif

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">Nema objavljenih priče</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop