@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Kategorije
        </div>

        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <th>
                    Ime kategorije
                </th>
                <th>

                </th>
                <th>

                </th>
                </thead>
                <tbody>
                @if($caregories->count())
                @foreach($caregories as $category)
                    <tr>
                        <td>
                            {{ $category->name }}
                        </td>
                        <td>
                            <a href="{{ route('category.edit', ['id'=> $category->id] ) }}" class="btn btn-success">
                                izmeni
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('category.delete', ['id'=> $category->id] ) }}" class="btn btn-danger">
                                obrisi
                            </a>
                        </td>
                    </tr>
                @endforeach
                @else
                    <tr>
                        <td colspan="5">Nema kategorija</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop