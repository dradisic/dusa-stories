@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')

    <div class="panel panel-default">
        <div class="panel-heading">
            Izmeni Kategoriju
        </div>
        <div class="panel-body">
            <form action="{{ route('category.update', ['id'=> $category->id] ) }}" method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">Ime</label>
                    <input type="text" name="name" value="{{ $category->name }}" class="form-control"/>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Izmeni</button>
                </div>
            </form>
        </div>
    </div>

@stop