@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')

    <div class="panel panel-default">
        <div class="panel-heading">
            Izmeni priču
        </div>
        <div class="panel-body">
            <form action="{{ route('post.update', ['id'=> $post->id] ) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="title">Naslov</label>
                    <input type="text" value="{{ $post->title }}" name="title" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="featured">Slika</label>
                    <input type="file" value="{{ $post->featured }}" name="featured" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="category_id">Izaberi Kategoriju</label>
                    <select id="category_id" value="{{ $post->category_id }}" name="category_id" class="form-control">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    @foreach($tags as $tag)
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="tags[]" value="{{ $tag->id }}"
                                   @foreach ($post->tags as $t)
                                        @if($tag->id == $t->id)
                                            checked
                                        @endif
                                    @endforeach
                                >
                                {{ $tag->tag }}
                            </label>
                        </div>
                    @endforeach
                </div>

                <div class="form-group">
                    <label for="content">Tekst</label>
                    <textarea name="content" id="content" cols="30" rows="20"
                              class="form-control">{{ $post->content }}</textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Izmeni</button>
                </div>
            </form>
        </div>
    </div>
@stop