@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Priče
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <th>
                    Slika
                </th>
                <th>
                    Ime priče
                </th>
                <th>

                </th>
                <th>

                </th>
                </thead>
                <tbody>
                @if($posts->count())
                    @foreach($posts as $post)
                        <tr>
                            <td>
                                <img src="{{ $post->featured }}" height="30px" alt="">
                            </td>
                            <td>
                                {{ $post->title }}
                            </td>
                            <td>
                                <a href="{{ route('post.edit', ['id'=> $post->id] ) }}" class="btn btn-success">
                                    izmeni
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('post.delete', ['id'=> $post->id] ) }}" class="btn btn-danger">
                                    obrisi
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">Nema objavljenih priče</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop