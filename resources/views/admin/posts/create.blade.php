@extends('layouts.app')

@section('content')
    @if(count($errors) > 0)
        <ul class="list-group">
            @foreach($errors->all() as $error)
                <li class="list-group-item text-danger">
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">
            Kreiraj priču
        </div>

        <div class="panel-body">
            <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Naslov</label>
                    <input type="text" name="title" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="featured">Slika</label>
                    <input type="file" name="featured" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="category_id">Izaberi Kategoriju</label>
                    <select id="category_id" name="category_id" class="form-control">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="tags">Izaberi Tag</label>
                    @foreach($tags as $tag)
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="tags[]" value="{{ $tag->id }}">{{ $tag->tag }}
                            </label>
                        </div>
                    @endforeach
                </div>

                <div class="form-group">
                    <label for="content">Tekst</label>
                    <textarea name="content" id="content" cols="30" rows="20" class="form-control"> </textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Kreiraj</button>
                </div>
            </form>
        </div>
    </div>
@stop