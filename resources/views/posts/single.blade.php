@extends('layouts.frontend')

@section('content')

    <div class="content-wrapper">

        <!-- Stunning Header -->

        <div class="stunning-header stunning-header-bg-lightviolet">
            <div class="stunning-header-content">
                <h1 class="stunning-header-title">{{ $post->title }}</h1>
            </div>
        </div>

        <!-- End Stunning Header -->

        <!-- Post Details -->


        <div class="container">
            <div class="row medium-padding120">
                <main class="main">
                    <div class="col-lg-10 col-lg-offset-1">
                        <article class="hentry post post-standard-details">

                            <div class="post-thumb">
                                <img src="{{ $post->featured }}" alt="{{ $post->title }}">
                            </div>

                            <div class="post__content">


                                <div class="post-additional-info">

                                    <span class="post__date">
                                        <i class="seoicon-clock"></i>
                                        <time class="published"
                                              datetime="{{ date('Y-m-d H:i:s', strtotime($post->created_at)) }}">
                                            {{ $post->created_at->toFormattedDateString() }}
                                        </time>
                                    </span>

                                    <span class="category">
                                            <i class="seoicon-tags"></i>
                                            <a href="#">
                                                {{ $post->category()->first()->name }}
                                            </a>
                                        </span>
                                </div>

                                <div class="post__content-info">

                                    {!! $post->content !!}

                                    <div class="padded-50"></div>
                                    <div class="widget w-tags">
                                        <div class="tags-wrap">
                                            @foreach($post->tags as $tag)
                                                <a href="#" class="w-tags-item">{{ $tag->tag }}</a>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="socials">Podeli priču:
                                <a href="#" class="social__item">
                                    <i class="seoicon-social-facebook"></i>
                                </a>
                                <a href="#" class="social__item">
                                    <i class="seoicon-social-twitter"></i>
                                </a>
                                <a href="#" class="social__item">
                                    <i class="seoicon-social-linkedin"></i>
                                </a>
                                <a href="#" class="social__item">
                                    <i class="seoicon-social-google-plus"></i>
                                </a>
                                <a href="#" class="social__item">
                                    <i class="seoicon-social-pinterest"></i>
                                </a>
                            </div>

                        </article>



                        <div class="pagination-arrow">
                            @if($prevPost)
                                <a href="{{ route('posts.single', ['slug' => $prevPost->slug]) }}"
                                   class="btn-prev-wrap">
                                    <svg class="btn-prev">
                                        <use xlink:href="#arrow-left"></use>
                                    </svg>
                                    <div class="btn-content">
                                        <div class="btn-content-title">Prethodna priča</div>
                                        <p class="btn-content-subtitle">{{ $prevPost->title }}</p>
                                    </div>
                                </a>
                            @endif
                            @if($nextPost)
                                <a href="{{ route('posts.single', ['slug' => $nextPost->slug]) }}"
                                   class="btn-next-wrap">
                                    <div class="btn-content">
                                        <div class="btn-content-title">Sledeća priča</div>
                                        <p class="btn-content-subtitle">{{ $nextPost->title }}</p>
                                    </div>
                                    <svg class="btn-next">
                                        <use xlink:href="#arrow-right"></use>
                                    </svg>
                                </a>
                            @endif

                        </div>

                        <div class="comments">

                            <div class="heading text-center">
                                <h4 class="h1 heading-title">Komentari</h4>
                                <div class="heading-line">
                                    <span class="short-line"></span>
                                    <span class="long-line"></span>
                                </div>
                            </div>
                            <div class="comments">
                                @foreach($comments as $comment)

                                    <div class="blog-details-author">

                                        <div class="blog-details-author-thumb">
                                            <img src="{{ asset('app/img/blog-details-author.png') }}" alt="Comment author">
                                        </div>

                                        <div class="blog-details-author-content">
                                            <div class="author-info">
                                                <h5 class="author-name">Philip Demarco</h5>
                                                <p class="author-info">SEO Specialist</p>
                                            </div>
                                            <p class="text">{{ $comment->comment }}
                                            </p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="panel-body">
                                <form action="{{ route('comment.store', ['id'=> $post->id ]) }}" method="post">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label for="comment">Komentar</label>
                                        <textarea name="comment" id="comment" cols="10" rows="5"
                                                  class="form-control"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-primary bg-greydark-color" type="submit">Kreiraj
                                        </button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                    <!-- End Post Details -->

                    <!-- Sidebar-->

                    <div class="col-lg-12">
                        <aside aria-label="sidebar" class="sidebar sidebar-right">
                            <div class="widget w-tags">
                                <div class="heading text-center">
                                    <h4 class="heading-title">Tagovi</h4>
                                    <div class="heading-line">
                                        <span class="short-line"></span>
                                        <span class="long-line"></span>
                                    </div>
                                </div>

                                <div class="tags-wrap">
                                    @foreach($post->tags as $tag)
                                        <a href="#" class="w-tags-item">{{ $tag->tag }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </aside>
                    </div>

                    <!-- End Sidebar-->

                </main>
            </div>
        </div>
    </div>

@endsection