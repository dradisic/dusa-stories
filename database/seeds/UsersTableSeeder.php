<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'name' => 'Dragan Radisic',
            'password' => bcrypt('35dusaAdmin'),
            'email' => 'scorrp@gmail.com',
            'admin' => 1,
        ]);
        App\Profile::create([
            'user_id' => $user->id,
            'about' => 'About Developer',
            'avatar' => 'uploads/avatars/default-avatar.png',
        ]);


    }
}
