<?php

namespace App\Http\Controllers;

use Actuallymab\LaravelComment\Commentable;
use Actuallymab\LaravelComment\Models\Comment;
use App\Setting;
use App\Post;
use App\Category;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index')
                ->with('settings', Setting::first())
                ->with('categories', Category::take(3)->get())
                ->with('first_post', Post::orderBy('created_at', 'desc')->first())
                ->with('second_post', Post::orderBy('created_at', 'desc')->skip(1)->take(1)->get()->first())
                ->with('third_post', Post::orderBy('created_at', 'desc')->skip(2)->take(1)->get()->first())
            ;
    }

    /**
     * Display the specified post.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function singlePost($slug)
    {
        $post = Post::where('slug', $slug)->first();
        return view('posts.single')
            ->with('title',$post->title)
            ->with('settings', Setting::first())
            ->with('categories', Category::take(3)->get())
            ->with('post', $post)
            ->with('comments', Comment::all()->where('commentable_id', $post->id))
            ->with('nextPost', $post->find(Post::where('id', '<', $post->id)->min('id')))
            ->with('prevPost', $post->find(Post::where('id', '>', $post->id)->max('id')))
            ;
    }

}
