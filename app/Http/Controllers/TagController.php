<?php

namespace App\Http\Controllers;

use App\Tag;
use Session;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.tags.index')->with('tags', Tag::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tag' => 'required|max:160',
        ]);

        $tag = Tag::create([
            'tag'=> $request->tag
        ]);

        Session::flash('success', 'Uspesno ste kreirali Tag.');

        return redirect()->route('tags');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::find($id);

        return view('admin.tags.edit')->with('tag', $tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tag' => 'required|max:160',
        ]);

        $tag = Tag::find($id);
        $tag->tag = $request->tag;

        $tag->save();

        Session::flash('success', 'Uspesno ste kreirali Tag.');

        return redirect()->route('tags');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tag::destroy($id);

        Session::flash('success', 'Uspesno ste obrisali Tag.');

        return redirect()->route('tags');
    }

    /**
     * Remove the trashed specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function kill($id)
    {
        $post = Tag::withTrashed()->where('id', $id)->first();
        $post->forceDelete();


        Session::flash('success', 'Tag je trajno obrisana');

        return redirect()->route('tag.trashed');
    }

    /**
     * Remove the trashed specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $post = Tag::withTrashed()->where('id', $id)->first();
        $post->restore();


        Session::flash('success', 'Tag je uspesno vracena');

        return redirect()->route('tag.trashed');
    }

    /**
     * Manage trashed resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed()
    {
        $posts = Tag::onlyTrashed()->get();

        return view('admin.tags.trashed')->with('tags', $posts);
    }
}
