<?php

namespace App\Http\Controllers;
use App\Setting;
use Illuminate\Http\Request;
use Session;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.settings')->with('setting', Setting::first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'site_name' => 'required',
            'contact_email' => 'required',
        ]);

        $setting = Setting::first();

        $setting->site_name = $request->site_name;
        $setting->contact_email = $request->contact_email;
        $setting->save();

        Session::flash('success', 'Uspesno ste podesili podesavanja');

        return redirect()->back();
    }
}
