<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index')->with('users', User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
        ]);

        $user = User::create([
            'name'=> $request->name,
            'email'=> $request->email,
            'password' => bcrypt('password')
        ]);
        $user->save();

        $profile = Profile::create([
            'user_id'=> $user->id,
            'avatar'=> 'uploads/avatars/default-avatar.png',
        ]);

        Session::flash('success', 'Uspesno ste kreirali usera: '. $user->name);

        return redirect()->route('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit')
            ->with('user', $user)
            ->with('profiles', Profile::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);

        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        Session::flash('success', 'Uspesno ste izmenili korisnika: '. $user->name);

        return redirect()->route('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::destroy($id);
        Session::flash('success', 'Uspesno ste obrisali korisnika: '. $user->name);

        return redirect()->route('users');
    }

    /**
     * Set user admin privileges
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function admin($id)
    {
        $user = User::find($id);
        $user->admin = 1;
        $user->save();
        Session::flash('success', 'Uspesno ste dodelili admin dozvole korisnika: '. $user->name);

        return redirect()->route('users');
    }

    /**
     * Set user admin privileges
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function notAdmin($id)
    {
        $user = User::find($id);
        $user->admin = 0;
        $user->save();
        Session::flash('success', 'Uspesno ste uklonili admin dozvole korisnika: '. $user->name);

        return redirect()->route('users');
    }
}
