<?php

namespace App;

use Actuallymab\LaravelComment\Commentable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Post extends Model
{
    use Commentable;

    use SoftDeletes;

    protected $mustBeApproved = true;

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'category_id', 'featured', 'slug'
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * @param $featured
     * @return string
     */
    public function getFeaturedAttribute($featured)
    {
        return asset($featured);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }


}
